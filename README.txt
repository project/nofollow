Nofollow module
===============

This is a quick and dirty module to allow you to nofollow menu items and taxonomy terms within drupal to help control the flow of pagerank through your site.

This is a technique that allows you to focus importance on certain pages rather than others - for example you may wish to avoid linking to the Privacy Policy on every page in the eyes of the search engines, but still allow users to find it from any part of the site. By reducing the importance of the Privacy policy, contact us/about us pages etc you allow more link juice to flow to the more important parts of the site.

Installation
============

The module should create its tables automatically when you enable it (though I am unsure of the syntax for pgsql so you will need to do it by hand there) however in order for it to have any effect you need to use custom theming functions.

These are very easy to set up - simply add the code below into your template.php file and replace yourtemplatename_ with the name of your template - eg bluemarine_

---------------------

function yourthemename_menu_item_link ($item,$link_item) {
	    $q=db_query("SELECT * FROM {nofollow} WHERE type='menu' AND id='$link_item[path]'");
			$ob=db_fetch_object($q);
			$dofollow=nofollow_dofollow_check($ob);
			$options=!empty($item['description']) ? array('title' => $item['description']) : array();
			if (!$dofollow) $options['rel']="nofollow";
	    return l($item['title']. " $isit", $link_item['path'], $options, isset($item['query']) ? $item['query'] : NULL);
}
	
function yourthemename_block ($block) {
  if (strstr($block->module, 'taxonomy_')) {
     // if this block is a taxonomy block of some sort - whether 
		 // taxonomy_context, taxonomy_menu etc
		 // then filter using patterns from nofollowlist
		 $block->content = preg_replace_callback('!<a.*?href="([^"]+)".*?>!', 'nofollow_link_replace', $block->content);
	}
  $output  = "<div class=\"block block-$block->module\" id=\"block-$block->module-$block->delta\">\n";
  $output .= " <h2 class=\"title\">$block->subject</h2>\n";
  $output .= " <div class=\"content\">$block->content</div>\n";
  $output .= "</div>\n";
  return $output;
}

---------------------------

If the template.php file does not exist please create an empty one, prefixing the code above with <?



Using nofollow.module
=====================

This module uses the same system as the block module's page specific visibility system to give you page specific nofollow control, so it should be familiar and easy to use.

To control whether menu items are nofollowed or not, simply edit the menu item. Below the submit button will be the Nofollow/Dofollow options. As with blocks you are given the following options:

* Dofollow on the listed page(s) only
* Dofollow on every page except the listed pages
* Dofollow when the following PHP code returns TRUE

This should give you full flexibility on where and when menu items are nofollowed or not. Note that you control if it is Dofollowed, not nofollowed

For taxonomy terms I wasn't entirely sure what to do as there are so many ways of presenting your categories to the user. Personally I use taxonomy_context and occasionally taxonomy_menu modules to display category information, so I have built the module around that.

Essentially it borrows the function from Nofollowlist which checks text for links, decides if it should be nofollowed or not, and applies it to the output of any block whose name begins 'taxonomy_' - its a bit of a kludge but it seems to work.

If there are other situations where there are links to categories you would like nofollowed then let me know and I'll work on including them.

To enable or disable nofollow just go to your taxonomy admin pages and 'edit term'. Once again you will get the familiar block visibility form to decide where and when nofollow is applied.

Credits
=======

Regular expressions and filter technique borrowed from Nofollowlist
Visibility options lifted from block.module

Cheers!